import matplotlib.pyplot as plt

nom_fichier='H:/Travail/info spé/salle 129.csv' #'emplacement/nom du fichier' il faut remplacer les antislash par les divise en python
flux=open(nom_fichier,'r') # on se met en mode read
ligne=flux.readline() #on va lire la ligne, il faut différencier readline et readlines! cette fonction permet d'arriver a la ligne suivante et de mettre dans ligne la suivante.

temps=[]
ay=[]
az=[]
ligne=flux.readline() #lecture de la 1ere ligne (apres entete)
while ligne!='':
    ligne=ligne.strip() #on enleve /n
    list=ligne.split(',') #  on casse en une liste de str
    temps.append(float(list[0]))
    ay.append(float(list[2]))
    az.append(float(list[3]))
    ligne=flux.readline()
flux.close()

#on va utiliser la methode d'euler pour intégrer l'acc
amoy=sum(ay)/len(ay)
vy,y=[0],[0]
for i in range(len(temps)-1):
    vy.append(vy[i]+(ay[i]-amoy)*(temps[i+1]-temps[i]))
    y.append(y[i]+vy[i]*(temps[i+1]-temps[i]))


plt.figure() #accélération
plt.plot(temps,ay,color='blue',label='ay')
plt.plot(temps,az,color='orange',label='az')
plt.legend()
plt.show()

plt.figure() # vitesse
plt.plot(temps,vy,color='green',label='vy')
plt.legend()
plt.show()

plt.figure()
plt.plot(temps,y,color='pink',label='y')
plt.legend()
plt.show()

modif
modif 2.0
modif 3.0
modif 4.0
